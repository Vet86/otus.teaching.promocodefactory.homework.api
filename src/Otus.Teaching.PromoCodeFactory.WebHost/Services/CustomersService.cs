﻿using Customers;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using static Customers.CustomersServiceGrpc;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomersService : CustomersServiceGrpcBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomerShortResponseListGrpc> GetCustomersAsync(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = new CustomerShortResponseListGrpc();
            response.Customers.AddRange(customers.Select(x => new CustomerShortResponseGrpc()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList());

            return response;
        }

        public override async Task<CustomerResponseGrpc> GetCustomerAsync(CustomerRequestGrpc request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            var response = new CustomerResponseGrpc()
            {
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Id = request.Id
            };
            response.Preferences.AddRange(
                customer.Preferences.Select(x => new PreferenceResponseGrpc()
                {
                    Id = x.Preference.Id.ToString(),
                    Name = x.Preference.Name
                }));

            return response;
        }

        public override async Task<CustomerResponseGrpc> CreateCustomerAsync(CreateOrEditCustomerRequestGrpc request, ServerCallContext context)
        {
            var model = new CreateOrEditCustomerRequest()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                PreferenceIds = request.PreferenceIds.Select(x => Guid.Parse(x)).ToList()
            };

            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(model.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(model, preferences);

            await _customerRepository.AddAsync(customer);

            return new CustomerResponseGrpc()
            {
                Id = customer.Id.ToString()
            };
        }

        public override async Task<Empty> EditCustomersAsync(EditCustomersRequestGrpc request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new IndexOutOfRangeException(nameof(customer));

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.Customer.PreferenceIds.Select(x => Guid.Parse(x)).ToList());


            var model = new CreateOrEditCustomerRequest()
            {
                Email = request.Customer.Email,
                FirstName = request.Customer.FirstName,
                LastName = request.Customer.LastName,
                PreferenceIds = request.Customer.PreferenceIds.Select(x => Guid.Parse(x)).ToList()
            };

            CustomerMapper.MapFromModel(model, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomerAsync(CustomerRequestGrpc request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new IndexOutOfRangeException(nameof(customer));

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}
